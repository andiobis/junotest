package com.andiobis.junosdktest.util


import java.util.*


open class ObserverHandler<T> {

    private var observers: Vector<(T?) -> Unit> = Vector()


    var observersSize = observers.size


    @Synchronized
    fun registerObserver(observer: (T?) -> Unit) {

        if (!observers.contains(observer)) {
            observers.add(observer)
        }
    }

    @Synchronized
    fun notifyDataSetChanged(value: T?) {

        observers.forEach {
            it(value)
        }
    }

    @Synchronized
    fun unregisterObserver(observer: (T?) -> Unit) {

        if (observers.contains(observer)) {
            observers.remove(observer)
        }
    }

}