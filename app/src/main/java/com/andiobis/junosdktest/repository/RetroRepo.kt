package com.andiobis.junosdktest.repository

import com.andiobis.junosdktest.app.Injection
import com.andiobis.junosdktest.model.Gist
import com.andiobis.junosdktest.model.Repo
import com.andiobis.junosdktest.model.TestModel
import com.andiobis.junosdktest.model.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

object RetroRepo : IRepository {

    lateinit var user: User

    private const val TAG = "RetroRepo"

    private const val LOGIN = "andiobis"

    private val api = Injection.provideGitHubApi()

    override suspend fun getRepos(): List<Repo>? {

        val response = api.getRepos(LOGIN)

        return if (response.isSuccessful) {

            response.body()
        } else {

            emptyList()

        }

    }

    override suspend fun getGists(): List<Gist>? {

        val response = api.getGists(LOGIN)

        return if (response.isSuccessful) {
            response.body()
        } else {

            emptyList()

        }
    }

    override suspend fun getUser(): User? {

        val response = api.getUser(LOGIN)

        return if (response.isSuccessful) {

            response.body()


        } else {

            null

        }
    }

    override fun getInfoAsync() = GlobalScope.async {

        val name = async {

            getUser()
        }


        val repo = async {

            getRepos()
        }

        val gist = async {

            getGists()
        }

        return@async TestModel(
            name = name.await()!!.name,
            firstRepo = repo.await()!![0].name,
            firstGist = gist.await()!![0].description
        )

    }


}


