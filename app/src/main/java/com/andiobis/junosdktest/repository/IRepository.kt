package com.andiobis.junosdktest.repository

import com.andiobis.junosdktest.model.Gist
import com.andiobis.junosdktest.model.Repo
import com.andiobis.junosdktest.model.TestModel
import com.andiobis.junosdktest.model.User
import kotlinx.coroutines.Deferred

interface IRepository {

    suspend fun getRepos(): List<Repo>?
    suspend fun getGists(): List<Gist>?
    suspend fun getUser(): User?
    fun getInfoAsync(): Deferred<TestModel>
}