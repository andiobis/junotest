package com.andiobis.junosdktest.repository

import android.net.Uri
import android.os.AsyncTask
import android.util.Log
import com.andiobis.junosdktest.app.Constants.fullUrlString
import com.andiobis.junosdktest.model.*
import com.google.gson.Gson
import kotlinx.coroutines.Deferred
import java.io.IOException

object BasicRepo : IRepository {
    override fun getInfoAsync(): Deferred<TestModel> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    private const val TAG = "BasicRepo"

    private const val LOGIN = "andiobis"

    private val gson = Gson()

    override suspend fun getRepos(): List<Repo>? {

        var liveData = mutableListOf<Repo>()
        FetchAsyncTask("/users/$LOGIN/repos", this::formatRepos) { repos ->

            repos?.repos?.let{

             liveData = it.toMutableList()

            }

        }.execute()

        return liveData
    }

    override suspend fun getGists(): List<Gist>? {
        var liveData = mutableListOf<Gist>()

        FetchAsyncTask("/users/$LOGIN/gists", this::formatGist) { gist ->


           gist?.gists?.let {

               liveData = it.toMutableList()

           }



        }.execute()

        return liveData
    }

    override suspend fun getUser(): User? {
        var liveData : User? = null

        FetchAsyncTask("/users/$LOGIN", this::formatUser) { user ->
            liveData = user
        }.execute()

        return liveData
    }

    private fun formatRepos(jsonString: String): Repos? {

        val final = "{ \"repos\": $jsonString}"

        return gson.fromJson(final, Repos::class.java)
    }

    private fun formatGist(jsonString: String): Gists? {
        val final = "{ \"gists\": $jsonString}"

        return gson.fromJson(final, Gists::class.java)
    }

    private fun formatUser(jsonString: String): User? {

        Log.i(TAG, jsonString)
        return gson.fromJson(jsonString, User::class.java)

    }

    private fun <T> fetch(path: String, formatter: (String) -> T): T? {

        try {
            val url = Uri.parse(fullUrlString(path)).toString()

            val jsonString = getUrlAsString(url)

            return formatter(jsonString)

        } catch (e: IOException) {
            Log.e(TAG, "Error info from server: ${e.localizedMessage}")
        }

        return null

    }

    private class FetchAsyncTask<T>(
        val path: String,
        val formatter: (String) -> T,
        val callback: (T) -> Unit
    ) : AsyncTask<(T) -> Unit, Void, T>() {


        override fun doInBackground(vararg p0: ((T) -> Unit)?): T? {
            return fetch(path, formatter)
        }

        override fun onPostExecute(result: T) {
            super.onPostExecute(result)
            if (result != null) {
                callback(result)
            }
        }

    }
}