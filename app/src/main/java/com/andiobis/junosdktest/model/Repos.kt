package com.andiobis.junosdktest.model


import com.andiobis.junosdktest.model.Repo
import com.google.gson.annotations.SerializedName

data class Repos(
    @SerializedName("repos")
    val repos: List<Repo>
)