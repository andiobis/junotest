package com.andiobis.junosdktest.model


import com.google.gson.annotations.SerializedName


data class Files(
    @SerializedName("vertical_recycler_item.xml")
    val verticalRecyclerItemxml: VerticalRecyclerItemxml
)