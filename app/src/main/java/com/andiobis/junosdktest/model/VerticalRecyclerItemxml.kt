package com.andiobis.junosdktest.model


import com.google.gson.annotations.SerializedName

data class VerticalRecyclerItemxml(
    @SerializedName("filename")
    val filename: String,
    @SerializedName("language")
    val language: String,
    @SerializedName("raw_url")
    val rawUrl: String,
    @SerializedName("size")
    val size: Int,
    @SerializedName("type")
    val type: String
)