package com.andiobis.junosdktest.model


import com.andiobis.junosdktest.model.Gist
import com.google.gson.annotations.SerializedName

data class Gists(
    @SerializedName("gists")
    val gists: List<Gist>
)