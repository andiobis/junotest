package com.andiobis.junosdktest.app

import android.app.Application
import android.content.Context
import com.google.firebase.FirebaseApp

class TestApp : Application() {

    companion object {
        private lateinit var instance: TestApp

        fun getAppContext(): Context = instance.applicationContext
    }

    override fun onCreate() {
        instance = this

        super.onCreate()
    }
}