package com.andiobis.junosdktest.app

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.andiobis.junosdktest.R
import com.andiobis.junosdktest.model.User
import com.andiobis.junosdktest.util.ObserverHandler
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity() {

    var user  = ObservableData<User>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        user.registerObserver(this::onUserChanged)

        GlobalScope.launch {

            val result = Injection.provideRepository().getUser()

            result?.let {
                user.value = it
            }
        }

    }

    override fun onResume() {
        super.onResume()

        user.registerObserver(this::onUserChanged)
    }


    override fun onPause() {
        super.onPause()

        user.unregisterObserver(this::onUserChanged)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onUserChanged(user : User?) {

        user?.let {

            Log.i("MainActivity", "Custom ${it.name}")
        }
    }


    class ObservableData<T>(
        initialValue : T? = null
    ) :ObserverHandler<T>(){

        var value: T? by Delegates.observable(initialValue){
                _, _, newValue ->

            notifyDataSetChanged(newValue)

        }
    }

}
