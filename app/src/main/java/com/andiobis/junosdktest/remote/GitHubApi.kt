package com.andiobis.junosdktest.remote

import com.andiobis.junosdktest.model.*
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubApi {

    @GET("users/{user}/repos")
    suspend fun getRepos(@Path("user") user : String) : retrofit2.Response<List<Repo>>

    @GET("users/{user}/gists")
    suspend fun getGists(@Path("user") user : String) : retrofit2.Response<List<Gist>>

    @GET("users/{user}")
    suspend fun getUser(@Path("user") user : String) : retrofit2.Response<User>

}